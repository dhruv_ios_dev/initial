//
//  ViewController.swift
//  Rapido
//
//  Created by DHRUV JAISWAL on 18/10/19.
//  Copyright © 2019 DHRUV JAISWAL. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class ViewController: UIViewController,UIGestureRecognizerDelegate,UISearchBarDelegate{
    let locationManager = CLLocationManager()
    @IBOutlet weak var cardViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var CardView: UIView!
    @IBOutlet weak var map: MKMapView!
    var Proglabel : UILabel?
    var but: UIButton?
    var userLocation = CLLocationCoordinate2D()
  var destinationLocation = CLLocationCoordinate2D()
    var storeAnno :[MKAnnotation] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
       
        searchBar.delegate = self
        let tap = UITapGestureRecognizer(target: self, action: #selector(pullUpView))
          tap.numberOfTapsRequired = 2
        tap.delegate = self
        CardView.addGestureRecognizer(tap)
        let tapMap = UITapGestureRecognizer(target: self, action: #selector(addLocation))
          tapMap.numberOfTapsRequired = 2
        tapMap.delegate = self
        map.addGestureRecognizer(tapMap)
        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(pullDown))
        swipe.direction = .down
        swipe.delegate = self
        CardView.addGestureRecognizer(swipe)
    }
    @objc func pullUpView(){
        print("in")
         cardViewHeight.constant = 300
        UIView.animate(withDuration: 0.4) {
             self.view.layoutIfNeeded()
        }
          collectionViewaddition()
    }
    @objc func pullDown(){
              print("out")
        cardViewHeight.constant = 55
        UIView.animate(withDuration: 0.4) {
            self.view.layoutIfNeeded()
        }
          }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        findAddress(add: searchBar.text!) { (succ) in
            if succ == true{
                print("succ")
            }
        }
    }
 


}
extension ViewController:CLLocationManagerDelegate,MKMapViewDelegate{
    func setup(){
        map.delegate = self
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let coord = locations.first?.coordinate{
            userLocation = CLLocationCoordinate2D(latitude: coord.latitude, longitude: coord.longitude)
            let region =  MKCoordinateRegion(center: userLocation, span: MKCoordinateSpan(latitudeDelta: 0.05, longitudeDelta: 0.05))
            map.setRegion(region, animated: true)
            let UserAnnotation = MKPointAnnotation()
            UserAnnotation.coordinate = userLocation
            UserAnnotation.title = "user"
            map.addAnnotation(UserAnnotation)
            locationManager.stopUpdatingLocation()
        }
    }
    @objc func addLocation(gs:UIGestureRecognizer){
        map.removeAnnotations(storeAnno)
        storeAnno.removeAll()
        let loc = gs.location(in: map)
        let coord = map.convert(loc, toCoordinateFrom: map)
        destinationLocation = coord
        let Newannotation = MKPointAnnotation()
        Newannotation.coordinate = coord
          Newannotation.title = "us"
        storeAnno.append(Newannotation)
        map.addAnnotation(Newannotation)
        distancebtw()
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation.title == "us"{
        var annotatioView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: "ss")
            annotatioView.pinTintColor = UIColor.green
            annotatioView.animatesDrop = true
            return annotatioView
        }
        if annotation.title == "user"{
            return nil
        }
        return MKPinAnnotationView()
    
    }
    func distancebtw(){
        let userCllocation = CLLocation(latitude: userLocation.latitude, longitude: userLocation.longitude)
        let desticllocation = CLLocation(latitude: destinationLocation.latitude, longitude: destinationLocation.longitude)
        var dist = userCllocation.distance(from: desticllocation)
        setupLabel(dis: String(((Int(dist / 1000)))))
        
    }
    func setupLabel(dis:String)
    {
        if Proglabel != nil{
            Proglabel?.removeFromSuperview()
        }
            Proglabel = UILabel()
            Proglabel!.frame = CGRect(x: 10, y: 10, width: 20, height: 20)
            Proglabel!.textColor = UIColor.white
            Proglabel!.text = dis
            CardView.addSubview(Proglabel!)
        addCallBtn()
        
    }
    func addCallBtn(){
        but = UIButton()
        but?.frame = CGRect(x: 300, y: 10, width: 70, height: 20)
        but?.setTitleColor(UIColor.black, for: .normal)
        but?.setTitle("R", for: .normal)
        but?.isUserInteractionEnabled = true
         but?.addTarget(self, action: #selector(TAP), for: .touchUpInside)
        CardView.addSubview(but!)
    }
    @objc func TAP(){
        print("ins")
   
       
    }
    func collectionViewaddition(){
        var collectionView = UICollectionView(frame:view.frame, collectionViewLayout: UICollectionViewFlowLayout())
        var nibName = UINib(nibName: "CarCollectionViewCell", bundle:nil)
        collectionView.register(nibName, forCellWithReuseIdentifier: "photoCell")
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.backgroundColor = UIColor.green
        collectionView.isScrollEnabled = true
        CardView.addSubview(collectionView)
        
        
    }
    func findAddress(add:String,handler:@escaping(Bool) -> ()){
        let geocoder = CLGeocoder()
              let address = add
                      geocoder.geocodeAddressString(address, completionHandler: {(placemarks, error) -> Void in
                          if((error) != nil){
                              print("Error", error ?? "")
                          }
                          if let placemark = placemarks?.first {
                              let coordinates:CLLocationCoordinate2D = placemark.location!.coordinate
                              handler(true)
                          }
                      })
    }
 
    
}
extension ViewController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "photoCell", for: indexPath) as? CarCollectionViewCell
        
        return cell!
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let w = view.bounds.size.width / 5
       collectionView.frame.size.width = w
        collectionView.frame.size.height = w * 2
        return CGSize(width: w, height: w)
        
        
    }
    
    
    
}
